#!/usr/bin/php
<?php

/**
 * @file
 * Install snippets, file templates and application templates.
 */

class manager {

  protected $handler_stdout = NULL;
  protected $handler_stderr = NULL;

  protected $kdehome = NULL;

  protected $components = array();

  protected $dir_katefiletemplates = NULL;
  protected $dir_kdevappwizard = NULL;
  protected $dir_ktexteditor_snippets = NULL;

  protected $dir_sourcebase = NULL;

  protected $snippet_repository_namespaces = array();

  public function __construct() {
    $this->handler_stdout = fopen('php://stdout', 'w');
    $this->handler_stderr = fopen('php://stderr', 'w');
    if (!$this->handler_stdout || !$this->handler_stderr) {
      throw new Exception();
    }

    $this->kdehomeInit();
    if (!$this->kdehome) {
      throw new Exception();
    }

    $this->dir_katefiletemplates = "{$this->kdehome}/share/apps/kate/plugins/katefiletemplates";
    $this->dir_kdevappwizard = "{$this->kdehome}/share/apps/kdevappwizard";
    $this->dir_ktexteditor_snippets = "{$this->kdehome}/share/apps/ktexteditor_snippets";
  }

  public function doit($op, $sourcebase) {
    if (!is_dir($sourcebase)) {
      throw new Exception();
    }
    $this->dir_sourcebase = $sourcebase;

    if ($op == 'install') {
      return $this->install();
    }
    elseif ($op == 'uninstall') {
      return $this->uninstall();
    }
    else {
      throw new Exception();
    }
  }

  protected function install() {
    // @todo escape $this->kdehome
    `mkdir --parent {$this->dir_katefiletemplates}/templates`;
    `mkdir --parent {$this->dir_kdevappwizard}/template_previews`;
    `mkdir --parent {$this->dir_kdevappwizard}/templates`;
    `mkdir --parent {$this->dir_ktexteditor_snippets}/data`;

    $this->install_katefiletemplates();
    $this->install_ktexteditor_snippets();
    $this->install_kdevappwizard();
  }

  protected function install_katefiletemplates() {
    $this->symlinks(
      "{$this->dir_sourcebase}/kate/plugins/katefiletemplates/templates",
      "{$this->dir_katefiletemplates}/templates",
      '/.+\.katetemplate$/'
    );
  }

  protected function install_ktexteditor_snippets() {
    $this->symlinks(
      "{$this->dir_sourcebase}/ktexteditor_snippets/data",
      "{$this->dir_ktexteditor_snippets}/data",
      '/.+\.xml$/'
    );
  }

  protected function install_kdevappwizard() {
    $dir = "{$this->dir_sourcebase}/kdevappwizard";
    $tmp = sys_get_temp_dir();

    $iterator = new DirectoryIterator($dir);
    foreach($iterator as $fileinfo) {
      $filename = $fileinfo->getFilename();
      if (!$fileinfo->isFile() || !preg_match('/.+\.inc$/', $filename)) {
        continue;
      }

      $basename = $fileinfo->getBasename('.inc');
      if (!is_file("$dir/$basename/$basename.kdevtemplate")) {
        continue;
      }

      $conf = NULL;
      include "$dir/$filename";
      if (!$conf) {
        continue;
      }

      `mkdir --parent $tmp/deploy/$basename`;

      $from_to = array(
        '{$templates}' => "{$this->dir_sourcebase}/kate/plugins/katefiletemplates/templates",
        '{$local}' => "$dir/$basename",
      );

      // @todo Get icon filename from *.kdevtemplate
      if (!empty($conf['icon'])) {
        $icon_src = strtr($conf['icon'], $from_to);
        $icon_dst = "{$this->dir_kdevappwizard}/template_previews/" . pathinfo($icon_src, PATHINFO_BASENAME);
        if (!is_file($icon_dst)) {
          copy($icon_src, $icon_dst);
        }
        else {
          //$this->message("Icon already exists: $icon_dst");
        }
      }

      $content_from_to = array(
        '${repoNamespace}' => '',
      );
      if (!empty($conf['snippet repository'])) {
        $content_from_to['${repoNamespace}'] = $this->snippet_repository_namespace($conf['snippet repository']);
      }

      if (is_array($conf['files']) && $conf['files']) {
        foreach ($conf['files'] as $src => $dst) {
          $src = strtr($src, $from_to);
          if (!is_file($src)) {
            // @todo ERROR
            $this->message($src);
            continue;
          }

          $dirname = pathinfo("$tmp/deploy/$basename/$dst", PATHINFO_DIRNAME);
          if (!is_file($dirname)) {
            `mkdir --parent $dirname`;
          }

          $content = file($src);
          foreach ($content as $i => $line) {
            if (strpos($line, 'katetemplate:') === 0) {
              unset($content[$i]);
            }
            else {
              break;
            }
          }

          file_put_contents("$tmp/deploy/$basename/$dst", strtr(implode('', $content), $content_from_to));
        }

        $cmd = sprintf('cd %s && tar --create ./* | bzip2 > %s',
          escapeshellarg("$tmp/deploy/$basename"),
          escapeshellarg("{$this->dir_kdevappwizard}/templates/$basename.tar.bz2")
        );
        `$cmd`;
        $this->message("{$basename}.tar.bz2");
      }
      `rm -R $tmp/deploy/$basename`;
    }
    `rm -R $tmp/deploy`;
  }

  protected function uninstall() {
    $this->message('@todo');
  }

  protected function snippet_repository_namespace($repository) {
    if (!isset($this->snippet_repository_namespaces[$repository])) {
      if (!is_file("{$this->dir_ktexteditor_snippets}/data/{$repository}.xml")) {
        $this->snippet_repository_namespaces[$repository] = '';
      }
      else {
        $doc = new DomDocument();
        $doc->loadXML(file_get_contents("{$this->dir_ktexteditor_snippets}/data/{$repository}.xml"));
        $e_snippets = $doc->getElementsByTagName('snippets')->item(0);
        $this->snippet_repository_namespaces[$repository] = ($e_snippets->hasAttribute('namespace') ?
          $e_snippets->getAttribute('namespace')
          :
          ''
        );
      }
    }
    return $this->snippet_repository_namespaces[$repository];
  }

  protected function symlinks($source, $destination, $pattern) {
    $iterator = new DirectoryIterator($source);
    foreach($iterator as $fileinfo) {
      $filename = $fileinfo->getFilename();
      if (!$fileinfo->isFile() || !preg_match($pattern, $filename)) {
        continue;
      }

      if (is_link("$destination/$filename")) {
        unlink("$destination/$filename");
      }

      symlink("$source/$filename", "$destination/$filename");
      $this->message($filename);
    }
  }

  protected function message($msg, $error = FALSE) {
    if ($error) {
      fwrite($this->handler_stderr, "$msg\n");
    }
    else {
      fwrite($this->handler_stdout, "$msg\n");
    }
  }

  protected function kdehomeInit() {
    if (isset($_SERVER['KDEHOME'])) {
      // @todo Check read-write
      $this->kdehome = is_dir($_SERVER['KDEHOME']) ? $_SERVER['KDEHOME'] : FALSE;
    }

    $home = NULL;
    if (isset($_SERVER['HOME'])) {
      $home = $_SERVER['HOME'];
    }
    elseif (isset($_SERVER['USER_HOME'])) {
      $home = $_SERVER['USER_HOME'];
    }

    if (!$home) {
      $this->kdehome = FALSE;
      return;
    }

    $this->kdehome = is_dir("$home/.kde4") ? "$home/.kde4" : FALSE;
  }
}

$op = ($argc > 1) ? $argv[1] : NULL;
$sourcebase = ($argc > 2) ? realpath($argv[2]) : getcwd();

try {
  $manager = new manager();
  $manager->doit($op, $sourcebase);
}
catch(Exception $e) {
  echo $e->getMessage(), "\n";
}

#!/usr/bin/php
<?php

/**
 * @file
 * Sort snippets.
 * Workaround for https://bugs.kde.org/show_bug.cgi?id=288049
 */

class SnippetSorter {
  protected $handler_stdout = NULL;
  protected $handler_stderr = NULL;

  protected $default_orderby = 'pms';
  protected $orderby = array();
  protected $orderby_count = 0;

  protected $orderby_aliases = array(
    'p' => 'displayprefix',
    'm' => 'match',
    's' => 'displaypostfix',
  );

  protected $items = array();

  public function help() {
    fwrite($this->handler_stdout,
      "Syntax\n" .
      "sort <filename> [orderby]\n" .
      "\n" .
      "Example:\n" .
      "sort ktexteditor_snippets/data/drupal-7.xml pms\n" .
      "\n" .
      "p = ascending <displayprefix>\n" .
      "P = descending <displayprefix>\n" .
      "m = ascending <match>\n" .
      "M = descending <match>\n" .
      "s = ascending <displaypostfix>\n" .
      "S = descending <displaypostfix>\n" .
      "Default orderby is: pms\n" .
      ""
    );
  }

  public function __construct() {
    $this->handler_stdout = fopen('php://stdout', 'w');
    $this->handler_stderr = fopen('php://stderr', 'w');
    if (!$this->handler_stdout || !$this->handler_stderr) {
      throw new Exception('Environment error.');
    }
  }

  public function doit($filename, $orderby) {
    if (!is_file($filename)) {
      $this->help();
      throw new Exception('File not exists');
    }

    if (!$orderby) {
      $orderby = $this->default_orderby;
    }

    $this->parse_orderby($orderby);

    $doc = new DomDocument();
    $doc->preserveWhiteSpace = TRUE;
    $doc->formatOutput = TRUE;

    if (!@$doc->loadXML(file_get_contents($filename))) {
      throw new Exception('Invalid XML');
    }

    $xpath = new DomXPath($doc);

    $e_snippets = $doc->childNodes->item(0);

    $xp_res_items = $xpath->query('/snippets/item');
    for ($i = 0; $i < $xp_res_items->length; $i++) {
      $e_item = $xp_res_items->item($i);
      $this->items[$i] = array(
        'node' => $e_item,
      );
      foreach ($this->orderby_aliases as $node_name) {
        $xp_res_field = $xpath->query($node_name, $e_item);
        $this->items[$i][$node_name] = ($xp_res_field->length ? $xp_res_field->item(0)->nodeValue : '');
      }

      $e_snippets->removeChild($e_item);
    }

    uasort($this->items, array($this, 'comparer'));

    foreach ($this->items as $item) {
      $e_snippets->appendChild($item['node']);
    }

    // @todo Check file is writeable
    file_put_contents($filename, $doc->saveXML());
  }

  public function comparer($a, $b) {
    $return = 0;
    $i = 0;
    while ($i < $this->orderby_count && $return == 0) {
      $node_name = $this->orderby[$i]['node_name'];
      if ($a[$node_name] == $b[$node_name]) {
        $return = 0;
        $i++;
        continue;
      }

      $return = ($a[$node_name] < $b[$node_name] ? -1 : 1);

      if (!$this->orderby[$i]['asc']) {
        $return = ($return == -1 ? 1 : -1);
      }
    }
    return $return;
  }

  protected function parse_orderby($orderby) {
    if (preg_match('/[^pms]/i', $orderby)) {
      throw new Exception('Invalid character in orderby');
    }

    $orderby_lower = strtolower(($orderby));
    if (
      substr_count($orderby_lower, 'p') > 1
      ||
      substr_count($orderby_lower, 'm') > 1
      ||
      substr_count($orderby_lower, 's') > 1
    ) {
      throw new Exception('Duplicated character in orderby');
    }

    for ($i = 0; $i < strlen($orderby); $i++) {
      $alias = $orderby[$i];
      $alias_lower = strtolower($alias);
      $this->orderby[] = array('node_name' => $this->orderby_aliases[$alias_lower], 'asc' => $alias == $alias_lower);
    }

    $this->orderby_count = count($this->orderby);
  }
}

try {
  $sorter = new SnippetSorter();
  $sorter->doit((!empty($argv[1]) ? $argv[1] : NULL), (!empty($argv[2]) ? $argv[2] : NULL));
  exit(0);
}
catch(Exception $e) {
  echo $e->getMessage(), "\n";
  echo $sorter->help();
  exit(1);
}

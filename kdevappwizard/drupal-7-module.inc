<?php

/**
 * @file
 * Application template installation settings.
 */
$conf = array(
  'snippet repository' => 'drupal-7',
  'icon' => '{$local}/drupal-7-module.png',
  'files' => array(
    '{$local}/drupal-7-module.kdevtemplate' => 'drupal-7-module.kdevtemplate',
    '{$templates}/drupal-7-mymodule.info.katetemplate' => '%{APPNAMELC}.info',
    '{$templates}/drupal-7-mymodule.module.katetemplate' => '%{APPNAMELC}.module',
    '{$templates}/drupal-7-mymodule.install.katetemplate' => '%{APPNAMELC}.install',
    '{$templates}/drupal-7-mymodule.drush.inc.katetemplate' => 'includes/%{APPNAMELC}.drush.inc',
  ),
);
